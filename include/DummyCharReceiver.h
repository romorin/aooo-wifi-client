#ifndef DUMMY_CHAR_RECEIVER_H
#define DUMMY_CHAR_RECEIVER_H

#include "CharReceiver.h"

#include "DebugPrint.h"

class DummyCharReceiver : public CharReceiver
{
public:
	DummyCharReceiver() {}
	virtual ~DummyCharReceiver() {}

	virtual bool receivedChar(const char data)
	{
		DEBUG_PRINT(data);
		return false;
	}

	virtual bool onConnectionEnd()
	{
		DEBUG_PRINTLN("Dummy Done!");
		return false;
	}
};
#endif
